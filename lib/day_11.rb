class Hourglasses
  def initialize(matrix:)
    @matrix = matrix
  end

  def max_sum
    max_hourglass.sum
  end

  private

  attr_reader :matrix

  def collection
    @collection ||= ExtractHourglassesFromMatrix.process(matrix: matrix)
  end

  def max_hourglass
    collection.max
  end
end

class Hourglass
  include Comparable

  def initialize(top:, center:, bottom:)
    @top    = top
    @center = center
    @bottom = bottom
  end

  def to_a
    [top, center, bottom]
  end

  def sum
    @sum ||= to_a.flatten.inject(0, :+)
  end

  private

  attr_reader :top, :center, :bottom

  def <=>(other)
    sum <=> other.sum
  end
end

class ExtractHourglassesFromMatrix
  def self.process(matrix:)
    new(matrix: matrix).process
  end

  def initialize(matrix:)
    @matrix = matrix
  end

  def process
    hourglass_row_values.flat_map do |i|
      hourglass_column_values.map do |j|
        Hourglass.new(    top: top_array(i, j),
                       center: center_array(i, j),
                       bottom: bottom_array(i, j))
      end
    end
  end

  private

  attr_reader :matrix

  def hourglass_row_values
    # Thank you zero based index
    # size - 2 => penultimate index value
    (1..(matrix.size-2))
  end

  def hourglass_column_values
    # Thank you zero based index
    # size - 2 => penultimate index value
    (1..(matrix[0].size-2))
  end

  def top_array(i, j)
    matrix[i-1][j-1..j+1]
  end

  def center_array(i, j)
    [ matrix[i][j] ]
  end

  def bottom_array(i, j)
    matrix[i+1][j-1..j+1]
  end
end
