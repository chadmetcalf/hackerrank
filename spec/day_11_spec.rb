require 'day_11'

RSpec.describe Hourglasses do
  subject(:hourglasses) { described_class.new(matrix: matrix) }

  it '#max_sum' do
    expect(subject.max_sum).to eq(19)
  end

  def matrix
    [[1, 1, 1, 0, 0, 0],
     [0, 1, 0, 0, 0, 0],
     [1, 1, 1, 0, 0, 0],
     [0, 0, 2, 4, 4, 0],
     [0, 0, 0, 2, 0, 0],
     [0, 0, 1, 2, 4, 0]]
  end
end
